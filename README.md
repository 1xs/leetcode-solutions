# LeetCode Solution Notes

## About
This project is an exercise I did while learning data structure.
At the same time, for the interview scene, both English and Chinese will be used to record ideas.

本项目是我在学习数据结构时做的练习。同时，针对面试场景，会同时使用英文和中文记录思路。
 
## Features

- Chinese and English bilingual record detailed solution ideas.
- complete runnable program
- sort by popularity
- record all solutions and gradually optimize.
- C++ / Python multilingual

---

- 中英双语详细记录解题思路
- 完整可运行程序
- 热度排序
- 记录所有解法，逐步优化
- C++ / Python 多语言

## Tips

mock interview, describe ideas in English.

more than half an hour without ideas, go discussion.

strategy to solve LeetCode problems.

1. hot
2. interview
3. tags / companies

review

- calculate complexity
- improve whiteboard handwriting speed.
- read your problem-solving ideas loudly and clearly.

##  Hot 100

Sort by number of solutions in descending order

> ✔ https://leetcode-cn.com/problemset/hot-100/ 

> https://leetcode.com/problemset/top-100-liked-questions/

| # | Title | Solution | Difficulty |
|---| ----- | -------- | ---------- |
|206|[Reverse Linked List](https://leetcode-cn.com/problems/reverse-linked-list/) | [C++]()|Easy|
|1|[Two Sum](https://leetcode-cn.com/problems/two-sum/) | [C++]()|Easy|
|2|[Add Two Numbers](https://leetcode-cn.com/problems/add-two-numbers/) | [C++]()|Medium|
|3|[Longest Substring Without Repeating Characters](https://leetcode-cn.com/problems/longest-substring-without-repeating-characters/) | [C++]()|Medium|
|20|[Valid Parentheses](https://leetcode-cn.com/problems/valid-parentheses/) | [C++]()|Easy|
|53|[Maximum Subarray](https://leetcode-cn.com/problems/maximum-subarray/solution/) | [C++](./Algorithms/53.Maximum-Subarray/)|Easy|


## Other Problems

### Array

| # | Title | Solution | Difficulty |
|---| ----- | -------- | ---------- |
|189|[Rotate Array](https://leetcode-cn.com/problems/rotate-array/) | [C++](./Algorithms/189.Rotate-Array/Solution.cpp)|Easy|

### Dynamic Programming
| # | Title | Solution | Difficulty |
|---| ----- | -------- | ---------- |
|1143|[Longest Common Subsequence](https://leetcode-cn.com/problems/longest-common-subsequence/) | [C++](./Algorithms/1143.Longest-Common-Subsequence/Solution.cpp)|Medium|


## Some useful links

https://leetcode.com/discuss/interview-question/448285/List-of-questions-sorted-by-common-patterns

https://leetcode.com/discuss/general-discussion/458695/dynamic-programming-patterns

https://medium.com/leetcode-patterns