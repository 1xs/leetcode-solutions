#include <iostream>
#include <vector>

using namespace std;

// Definition for singly-linked list.
struct ListNode
{
    int val;
    ListNode *next;
    // it's a contructor, which uses initialization list.
    // https://en.cppreference.com/w/cpp/language/initializer_list
    ListNode(int x) : val(x), next(NULL) {}
};

// iterative solution
class IterativeSolution
{
public:
    ListNode *reverseList(ListNode *head)
    {
        if (!head)
        {
            return nullptr;
        }
        // set prev pointer to NULL
        ListNode *prev = NULL;
        // set curr pointer to head
        ListNode *curr = head;
        while (curr != NULL)
        {
            // temporarily store curr pointer to next
            ListNode *nextTemp = curr->next;
            // reverse pointer, move the next of curr node as prev
            curr->next = prev;
            // move backward
            prev = curr;
            curr = nextTemp;
        }
        // curr is pointer to NULL, prev is pointer to last node(new head)
        return prev;
    }
};

// recursive solution
class RecursiveSolution
{
public:
    ListNode *reverseList(ListNode *head)
    {

    }
};